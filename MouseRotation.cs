﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MouseRotation : MonoBehaviour
{
    //check if some cant be put as PRIVATE and initialized in c++

    private float lookSensitivity, xRot;
    private Transform playerTrans;

    // Start is called before the first frame update
    void Start()
    {
        Cursor.lockState = CursorLockMode.Locked;
        playerTrans = GameObject.Find("Player").transform;
        lookSensitivity = 300;
        xRot = 0;
    }

    // Update is called once per frame
    void Update()
    {
        float mouseX = Input.GetAxis("Mouse X") * lookSensitivity * Time.deltaTime;
        float mouseY = Input.GetAxis("Mouse Y") * lookSensitivity * Time.deltaTime;

        xRot -= mouseY;
        xRot = Mathf.Clamp(xRot, -80f, 45f);

        transform.localRotation = Quaternion.Euler(xRot, 0, 0);

        playerTrans.transform.Rotate(Vector3.up * mouseX);
    }

}
