﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class EnemyClass : MonoBehaviour
{
    private PlayerClass player;
    private BarsCode bars;
    private float life;
    private CombatMechanics combat;
    public static float damage;
    public float distance;

    // Start is called before the first frame update
    void Start()
    {
        bars = GameObject.Find("_Scripts").GetComponent<BarsCode>();
        player = GameObject.Find("Player").GetComponent<PlayerClass>();
        combat = GameObject.Find("_Scripts").GetComponent<CombatMechanics>();
        life = 1; 
        
    }

    // Update is called once per frame
    void Update()
    {
        distance = Vector3.Distance(player.transform.position, gameObject.transform.position); 
        Die();
    }

    public void Die()
    {
        if (life <= 0)
        {
            Destroy(gameObject);
        }
    }

    public void TakeHit(float d)
    { 
        Debug.Log("enemy got hit");
        d = combat.eDamage;
        life -= d;
    }

    private void OnCollisionEnter(Collision other) // attakes player
    {
        if (other.gameObject.name.Contains("Player")){
                    bars.SetHealth(combat.pDamage);
        }
    }

}
