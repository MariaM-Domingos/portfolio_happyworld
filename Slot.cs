﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/*This class purelly checks if the inventory has any child 
 * besides 1, that is the panel
 * as the collectables are stored as childs of the slots
 * if they dont have any, make them automatically empty*/

public class Slot : MonoBehaviour
{ 
    public int i;
    private InventoryClass inventory;
    // Start is called before the first frame update
    void Start()
    {
        inventory = GameObject.Find("Player").GetComponent<InventoryClass>();
    }

    // Update is called once per frame
    void Update()
    {
            if (transform.childCount <=1)
            {
                inventory.isFull[i] = false;
            }
    }
}
