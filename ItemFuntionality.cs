﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine;

public class ItemFuntionality : MonoBehaviour
{
    private Text text;
    private PlayerClass player;
    private DualDoors[] doors;
    private float pillsAdd;

    // Start is called before the first frame update
    void Start()
    {
        player = GameObject.Find("Player").GetComponent<PlayerClass>();
        doors = FindObjectsOfType<DualDoors>();
        pillsAdd = .2f;
        text = GameObject.Find("Message").GetComponent<Text>();
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void UseKey()
    {
        for (int i = 0; i < doors.Length; i++)
        {
            if (doors[i].distance < 3.5 && doors[i].locked == true)
            {
                doors[i].locked = false;
                doors[i].DoorState("Open");
                text.enabled = true;
                text.text = "Okeey... it works here..";
                Destroy(gameObject);

            }            
        }
    }
    public void UsePill()
    {
        player.BoughtPills(pillsAdd);        
        Destroy(gameObject);
    }


   
}
