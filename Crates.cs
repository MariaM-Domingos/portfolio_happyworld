﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Crates : MonoBehaviour
{
    private float distance;
    private GameObject player;
    private int randVal;
    // Start is called before the first frame update
    void Start()
    {
        player = GameObject.Find("Player");
        randVal = Random.Range(1, 4);
    }

    // Update is called once per frame
    void Update()
    {
        distance = Vector3.Distance(player.transform.position, gameObject.transform.position);
        DestroyCrate();
    }
    private void DestroyCrate()
    {
        if(distance < 2)
        {
            if (Input.GetKeyDown(KeyCode.E))
            {
                PlayerClass.coins += randVal;
                Destroy(gameObject);
            }
        }
    }
}
