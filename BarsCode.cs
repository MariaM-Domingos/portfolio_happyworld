﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class BarsCode : MonoBehaviour
{
    private Slider healthBar, medBar;
   
    // Start is called before the first frame update
    void Start()
    {
        healthBar = GameObject.Find("HealthBar").GetComponent<Slider>();
        medBar = GameObject.Find("MedBar").GetComponent<Slider>();
    }

    // Update is called once per frame
    void Update()
    {
       
    }

    public void SetHealth(float value)
    {
        if (PlayerClass.health > 0)
        {
            PlayerClass.health -= value;
            healthBar.value = PlayerClass.health;
        }
        else
        {
            PlayerClass.health = 0;
        }
       
    }

    public void SetMedication(float value)
    {
        if(PlayerClass.medication > 0)
        {
            PlayerClass.medication -= value;
            medBar.value = PlayerClass.medication;
        }
        else
        {
            PlayerClass.medication = 0;
        }
            
       
    }
}
