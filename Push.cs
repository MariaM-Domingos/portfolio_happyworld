﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine;

public class Push : MonoBehaviour
{
    public VendingMachines machine;
    private Text text;
    private GameObject player;
    private Rigidbody _machineRB;
    private float distance;

    // Start is called before the first frame update
    void Start()
    {
        player = GameObject.Find("Player");
        _machineRB = machine.GetComponent<Rigidbody>();
        text = GameObject.Find("Message").GetComponent<Text>();
    }

    // Update is called once per frame
    void Update()
    {
        distance = Vector3.Distance(player.transform.position, gameObject.transform.position);

        if (distance <1 )
        {
            pushDirection();
        }
       
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.name == "Player")
        {
            text.enabled = true;
            text.text = "Maybe a little push?...";
        }
    }
    private void OnTriggerExit(Collider other)
    {
        text.enabled = false;
    }

    private void pushDirection()
    {
        Vector3 pushDir = (machine.transform.position - player.transform.position).normalized;
        if (Input.GetKey(KeyCode.E))
        {
            _machineRB.isKinematic = false;
            _machineRB.AddForce(Vector3.back);
        }
        else
        {
            _machineRB.isKinematic = true;
        }
    }
}
