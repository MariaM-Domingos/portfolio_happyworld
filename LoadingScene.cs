﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using UnityEngine;


public class LoadingScene : MonoBehaviour
{
    private bool loadScene, canLoad;
    private Slider bar;
    private float timer;
    // Start is called before the first frame update
    void Start()
    {
        loadScene = false;
        bar = GameObject.Find("LoadingBar").GetComponent<Slider>();
    }

    // Update is called once per frame
    void Update()
    {
        if (!loadScene)
        {
            loadScene = true;
            StartCoroutine(LoadYourAsyncScene());
        }
        
    }
    IEnumerator LoadYourAsyncScene()
    {
        while ( timer < 1)
        {
            float randNumb = Random.Range(.1f, .3f);
            timer += randNumb;
            bar.value += randNumb;
            randNumb = Random.Range(.2f, .5f);
            yield return new WaitForSeconds(randNumb);

        }
        if (timer > 1)
        {
            canLoad = true;
        }
        // Wait until the asynchronous scene fully loads
        if (canLoad) {
            AsyncOperation asyncLoad = SceneManager.LoadSceneAsync("GameScene");
            while (!asyncLoad.isDone)
            {

                yield return null;
            }

        }

    }
       
}

