﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine;

public class PlayerClass : MonoBehaviour
{
    //check if some cant be put as PRIVATE and initialized in c# 
    private Rigidbody _player;
    private BarsCode bars;
    private Text coinsText;
    private GameFinaleScreen game;
    //player meters values
    private float maxBars, jumpVal, speed;
    public static float health, medication, coins; // 1 = 100
    public static Vector3 position;

    // Start is called before the first frame update
    void Start()
    {
        maxBars = 1;
        health = 1;
        medication = 0.75f;
        jumpVal = 2;
        coins = 10;
        _player = GetComponent<Rigidbody>();
        bars = GameObject.Find("_Scripts").GetComponent<BarsCode>();
        game = GameObject.Find("_Scripts").GetComponent<GameFinaleScreen>();
        coinsText = GameObject.Find("CoinsVal").GetComponent<Text>();
    }

    // Update is called once per frame
    void Update()
    {
        position = transform.position;
     
        //to run
        if (Input.GetKey(KeyCode.LeftShift))
        {
            speed = 2.2f;
             MovePlayer();
        }
        else
        {
            speed = 1;
            MovePlayer();
        }

        Attack();

        DeployMed();
        DeployHealth();

        coinsText.text = coins.ToString();
    }
    void MovePlayer()
    {
        
        // move right  D
        if (Input.GetKey(KeyCode.D))
        {
            transform.Translate(Vector3.right * speed * Time.deltaTime );
        }

        //move left  A
        if (Input.GetKey(KeyCode.A))
        {
            transform.Translate(Vector3.left * speed * Time.deltaTime );
        }
        //move up  space
        if (Input.GetKeyDown(KeyCode.Space))
        {
            _player.AddForce(Vector3.up * jumpVal, ForceMode.Impulse);
        }

        //move foward W
        if (Input.GetKey(KeyCode.W))
        {
            transform.Translate(Vector3.forward * speed * Time.deltaTime );
        }

        //move backward S
        if (Input.GetKey(KeyCode.S))
        {
            transform.Translate(Vector3.back * speed * Time.deltaTime );
        }
    }
    public void Attack()
    {
        RaycastHit hit;
        if (Physics.Raycast(transform.position, transform.forward, out hit, 2))
        {
            if (Input.GetMouseButtonDown(0))
            {
                if (hit.collider.name.Contains("Enemy"))
                {
                    EnemyClass enemy = hit.collider.GetComponent<EnemyClass>();
                    enemy.TakeHit(.2f);
                }
            }
            
        }
    } //Player will attack enemy when we click the left mouse button
    public void Buy(float val)
    {
        coins -= val;
    }
    public void BoughtPills(float val)
    {
        float value = medication += val;
        if (value >= maxBars)
        {
            medication = value;
        }
        else
        {
            medication += val;
        }
    }
    public void BoughtHealth(float val)
    {
        float value = health += val;
        if (value >= maxBars)
        {
            health = value;
        }
        else
        {
            health += val;
        }
    }
    public void DeployMed() {
        if ( Time.deltaTime < 1)
        {
            //IF THE PLAYER IS ALIVE TAKES X Point(s) FROM PLAYERS MEDICATION LEVEL 
            float x = 0.0002f;
            bars.SetMedication(x);
        }
    }
    public void DeployHealth()
    {
        if( medication <= 0)
        {
            float x = 0.0003f;
            bars.SetHealth(x);
        }
    }
    private void OnTriggerEnter(Collider other)
    {
        if (other.name.Contains("EXIT"))
        {
            if (TaskClass.tasksDone == 7)
            {
                game.GameWon();
            }
        }
       
    }
}
