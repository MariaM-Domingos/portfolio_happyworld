﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;

public class DataPersistence : MonoBehaviour
{
    private GameObject player;
    private InventoryClass inventory;
    private struct PlayerDetails
    {
        public float coins;
        public float life;
        public float medication;
        public Vector3 position;
        public int[] items;
    }
    public CollectablesClass[] prefabs;
    // Start is called before the first frame update
    void Start()
    { 
        player = GameObject.Find("Player");
        inventory = GameObject.Find("Player").GetComponent<InventoryClass>();
        LoadCode();
    }

    // Update is called once per frame
    void Update()
    {


    }

    void SaveCode()
    {
        Debug.Log("Saved");
        PlayerDetails playerDetails = new PlayerDetails
        {
            coins = PlayerClass.coins,
            life = PlayerClass.health,
            medication = PlayerClass.medication,
            position = PlayerClass.position,
            items = inventory.itemsID,
          };

        
        string jsonString = JsonUtility.ToJson(playerDetails);

            File.WriteAllText(Application.dataPath + "/DataPersistence.json", jsonString);
    }

    void LoadCode()
    {
        Debug.Log("Loaded");
        if (File.Exists(Application.dataPath + "/DataPersistence.json"))
        {
            string saveString = File.ReadAllText(Application.dataPath + "/DataPersistence.json");

            PlayerDetails playerDetails = JsonUtility.FromJson<PlayerDetails>(saveString);
            PlayerClass.coins = playerDetails.coins;
            player.gameObject.transform.position = playerDetails.position;
            PlayerClass.health = playerDetails.life;
            PlayerClass.medication = playerDetails.medication;

            for (int i = 0; i < inventory.slots.Length; i++)
            {
                if(prefabs[0].code == playerDetails.items[i])
                {
                    inventory.isFull[i] = true;
                    Instantiate(prefabs[0].itemButton, inventory.slots[i].transform, false);
                    inventory.itemsID[i] = playerDetails.items[i];
                }
                if (prefabs[1].code == playerDetails.items[i])
                {
                    inventory.isFull[i] = true;
                    Instantiate(prefabs[1].itemButton, inventory.slots[i].transform, false);
                    inventory.itemsID[i] = playerDetails.items[i];
                }
                if (prefabs[2].code == playerDetails.items[i])
                {
                    inventory.isFull[i] = true;
                    Instantiate(prefabs[2].itemButton, inventory.slots[i].transform, false);
                    inventory.itemsID[i] = playerDetails.items[i];
                }
            }
        }
        
    }

    
}
