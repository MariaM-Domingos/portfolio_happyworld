﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine;

public class DualDoors : MonoBehaviour
{
    private Text text;
    private Animator animator;
    private bool doorOpen;
    public float distance;
    public bool locked;

    // Start is called before the first frame update
    private void Start()
    {
        doorOpen = false;
        animator = GetComponent<Animator>();
        text = GameObject.Find("Message").GetComponent<Text>();
    }

    // Update is called once per frame
    private void Update()
    {
        distance = Vector3.Distance(GameObject.Find("Player").transform.position, gameObject.transform.position);
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag == "Player")
        {
            if(locked){
                doorOpen = false;
                //DoorState("Close");
            }
            else
            {
                doorOpen = true;
                DoorState("Open");

            }
            
        }
    }
    private void OnTriggerExit(Collider other)
    {
        if (doorOpen)
        {
            doorOpen = false;
            DoorState("Close");
        }
        text.enabled = false;
    }

    public void DoorState(string condition){
        animator.SetTrigger(condition);
    }
}
