﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CombatMechanics:MonoBehaviour
{
    private PlayerClass player;
    public float eDamage,pDamage;
    // Start is called before the first frame update
    void Start()
    {
        player = GameObject.Find("Player").GetComponent<PlayerClass>();
       
        pDamage = 0;
        eDamage = 0;
    }

    // Update is called once per frame
    void Update()
    {
        SetEDamage();
        SetPDamage();
    }

    float SetEDamage() // quantivy of damage enemy takes if med bar is X value
    {
        //if (player.bars.medBar.value > 0.75)
        //{
        //    eDamage = 0.1f;
        //    return eDamage;
        //} else if(player.bars.medBar.value < 0.5)
        //{
            eDamage = 0.2f;
            return eDamage;
        //}
        //else if(player.bars.medBar.value < 0.35)
        //{
        //    eDamage = 0.333f;
        //}
        //return 0;
    }
    float SetPDamage() // quantivy of damage player takes if med bar is X value
    {
        //if (player.bars.medBar.value > 0.75)
        //{
        //    pDamage = 0.333f;
        //    return pDamage;
        //}
        //else if (player.bars.medBar.value < 0.5)
        //{
            pDamage = 0.2f;
            return pDamage;
        //}
        //else if (player.bars.medBar.value < 0.35)
        //{
        //    pDamage = 0.1f;
        //}
        //return 0;
    }
}
