﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CollectablesClass : MonoBehaviour
{
    public GameObject itemButton;
    public int code;
    private InventoryClass inventory;

    // Start is called before the first frame update
    void Start()
    {
        inventory = GameObject.Find("Player").GetComponent<InventoryClass>();
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.name.Contains("Player"))
        {
            for (int i = 0; i < inventory.slots.Length; i++)
            {
                if(inventory.isFull[i] == false)
                {
                    //Item can be added to Inventory!
                    inventory.isFull[i] =true;
                    Instantiate(itemButton,inventory.slots[i].transform, false);
                    inventory.itemsID[i] = code;
                    Destroy(gameObject);
                    break;
                }
            }
        }
    }
}

//https://www.flaticon.com/authors/nhor-phai ICONS AUTHOR