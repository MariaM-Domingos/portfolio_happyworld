﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine;

public class VendingMachines : MonoBehaviour
{
    public InventoryClass inventory;
    public CollectablesClass pills;
    private Text text;
    private PlayerClass player;
    private float healthAdd, distance;
    private float pricesPill, pricesHealth;
    // Start is called before the first frame update
    void Start()
    {
        healthAdd = .2f;
        player = GameObject.Find("Player").GetComponent<PlayerClass>();
        inventory = GameObject.Find("Player").GetComponent<InventoryClass>();
        text = GameObject.Find("Message").GetComponent<Text>();
        pricesPill = 2;
        pricesHealth = 5;
    }

    // Update is called once per frame
    void Update()
    {
        distance = Vector3.Distance(player.transform.position, gameObject.transform.position);
        if (distance <2)
        {
            addPurchase();
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.name.Contains("Player") && PlayerClass.coins < pricesPill )
        {
            text.enabled = true;
            text.text = "Hmm... I don't have enough coins, maybe I can find more around the warehouse..";
           
        }
    }
    //Create a code to check if the player decided to buy what... and than take the price
    private void OnTriggerExit(Collider other)
    {
        text.enabled = false;
    }
    void addPurchase()
    {
        if (PlayerClass.coins > 1)
        {
            if (Input.GetKeyDown(KeyCode.Alpha1))
            {
                for (int i = 0; i < inventory.slots.Length; i++)
                {
                    if (inventory.isFull[i] == false)
                    {
                        //Item can be added to Inventory!
                        inventory.isFull[i] = true;
                        Instantiate(pills.itemButton, inventory.slots[i].transform, false);
                        inventory.itemsID[i] = pills.code;
                        break;
                    }
                }
                PlayerClass.coins -= pricesPill;
            } else if (Input.GetKeyDown(KeyCode.Alpha2))
            {
                player.BoughtHealth(healthAdd);
                PlayerClass.coins -= pricesHealth;
            }
        }
        
    }
}
