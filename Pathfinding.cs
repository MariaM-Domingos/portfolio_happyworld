﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class Pathfinding : MonoBehaviour
{
    private GameObject target, player;
    public NavMeshAgent agent;

    // Start is called before the first frame update
    void Start()
    {
        player = GameObject.Find("Player");
        target = GameObject.Find("Target");
        
   }

    // Update is called once per frame
    void Update()
    {       
       StartPathFinding();
    }

    void StartPathFinding()
    {
        float distance = Vector3.Distance(player.transform.position, transform.position);
        if (distance < 6)
        {
            agent.SetDestination(player.transform.position);
            agent.transform.LookAt(player.transform.position);
        }
        else
        {
            agent.SetDestination(target.transform.position);
        }
    }
}
