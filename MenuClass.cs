﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class MenuClass : MonoBehaviour
{
    private bool paused, isActive;
    public GameObject creditsMenu, mainMenu, pauseMenu, gameOver, inventory;
    // Start is called before the first frame update
    void Start()
    {
        isActive = false;
        Time.timeScale = 1;
        paused = false;
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            TogglePause();
        } else 
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            TogglePause();
        }
        //to see tasks and inventory
        ToggleInventory();
    }
    public void StartGame()
    {
       SceneManager.LoadScene(1); //1 is respctive to Loading Scene, this is used to load different scenes

    }
    public void ToggleCredits()
    {
        if (mainMenu.activeInHierarchy)
        {
            creditsMenu.SetActive(true);
            mainMenu.SetActive(false);

            Debug.Log("Pressed Credits");
        }
        else
        {
            creditsMenu.SetActive(false);
            mainMenu.SetActive(true);
        }
    }
    public void TogglePause()
    {

        paused = !paused;
        if (paused) //if pause is true
        {
            Cursor.visible = paused;
            Cursor.lockState = CursorLockMode.None;
            pauseMenu.SetActive(paused);
            Time.timeScale = 0;
        }
        else
        {
            Cursor.visible = paused;
            Cursor.lockState = CursorLockMode.None;
            pauseMenu.SetActive(paused);
            Time.timeScale = 1;
        }
    }
    public void Quit()
    {
        Application.Quit();
    }
    public void ReturnToMainMenu()
    {
        SceneManager.LoadScene(0); //0 is respctive to Main Menu, this is used to load different scenes
        Time.timeScale = 1;
    }
    public void ToggleInventory() // Click tab to see the inventory and player's tasks
    {
        if (Input.GetKeyDown(KeyCode.Tab))
        {

            isActive = !isActive;
            if (isActive) //if pause is true
            {
                Cursor.visible = true;
                Cursor.lockState = CursorLockMode.None;
                inventory.SetActive(true);
                //tasks.SetActive(true);
                Time.timeScale = 0;
            }
            else
            {
                Cursor.visible = false;
                inventory.SetActive(false);
                //tasks.SetActive(false);
                Time.timeScale = 1;
            }
        }
    }
}
