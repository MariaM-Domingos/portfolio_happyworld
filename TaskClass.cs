﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine;

public class TaskClass : MonoBehaviour
{
    private bool[] complete;
    private string[] tasks = { 
        "Find yourself a uniform",  
        "Explore the warehouse",
        "Find the Office door",
        "Find the Cafeteria",
        "Find the Managers Office",
        "Find the Laboratory?",
        "Find anwesers.",
        "Go home.."
    };
    public Text text;
    public static int tasksDone, totalTasks;
    // Start is called before the first frame update
    void Start()
    {
        text.text = tasks[0];
        complete = new bool[tasks.Length];
        tasksDone = 0;
        for (int i = 0; i < tasks.Length; i++)
        {
            complete[i] = false; //task not complete
        }
    }

    // Update is called once per frame
    void Update()
    {

    }

   private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.name == "Clothes" && tasksDone == 0) // 1
        {
            complete[0] = true;
            if (complete[0])
            {
                text.text = tasks[1];
            }
            tasksDone += 1;
        }
        if (other.gameObject.name == "OfficeKey" && tasksDone == 1) // 2
        {
            complete[1] = true;
            if (complete[1])
            {
                text.text = tasks[2];
            }
            tasksDone += 1;
        }
        if (other.gameObject.name == "MasterRoom Key" && tasksDone == 2) //3
        {
            complete[2] = true;
            if (complete[2])
            {
                text.text = tasks[3];
            }
            tasksDone += 1;
        }
        if (other.gameObject.name == "FOffice Key" && tasksDone == 3) //4
        {
            complete[3] = true;
            if (complete[3])
            {
                text.text = tasks[4];
            }
            tasksDone += 1;
        }
        if (other.gameObject.name == "SecretKey" && tasksDone == 4) // 5
        {
            complete[4] = true;
            if (complete[4])
            {
                text.text = tasks[5];
            }
            tasksDone += 1;
        }
        if (other.gameObject.name == "Secret Door" && tasksDone==5) //6
        {
            complete[5] = true;
            if (complete[5])
            {
                text.text = tasks[6];
            }
            tasksDone += 1;
        }
        if (other.gameObject.name == "Truth" && tasksDone == 6)//7
        {
            complete[6] = true;
            if (complete[6])
            {
                text.text = tasks[7];
                Destroy(other.gameObject);
            }
            tasksDone += 1;
        }
    }

    private void OnTriggerExit(Collider other)
    {
        
    }
}
